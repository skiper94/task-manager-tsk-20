package ru.apolyakov.tm.service;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.service.ITaskService;
import ru.apolyakov.tm.exception.empty.EmptyDescriptionException;
import ru.apolyakov.tm.exception.empty.EmptyIdException;
import ru.apolyakov.tm.exception.empty.EmptyNameException;
import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.exception.system.ComparatorIncorrectException;
import ru.apolyakov.tm.exception.system.IndexIncorrectException;
import ru.apolyakov.tm.exception.system.StatusIncorrectException;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.apolyakov.tm.util.CheckUtil.*;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String name, final String description, final String userId){
        if(isEmpty(name)) throw new EmptyNameException();
        if(isEmpty(description)) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name,final String userId){
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeOneByName(name,userId);
    }

    @Override
    public void removeOneById(final String id,final String userId){
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task findOneById(final String id,final String userId){
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findOneById(id, userId);
    }

    @Override
    public Task findOneByIndex(final Integer index,final String userId){
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index,userId);
    }

    @Override
    public Task findOneByName(final String name,final String userId){
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findOneByName(name,userId);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description,final String userId){
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneById(id,userId);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskByIndex(final Integer index,final String userId){
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeOneByIndex(index,userId);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description,final String userId){
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByIndex(index,userId);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByName(final String name, final String nameNew, final String description,final String userId) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        final Task task = findOneByName(name,userId);
        if (task == null) throw new TaskNotFoundException();
        task.setName(nameNew);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id,final String userId) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findOneById(id,userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index, final String userId) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = finishTaskByIndex(index,userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name, final String userId) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = finishTaskByName(name, userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id, final String userId) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findOneById(id, userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index, final String userId) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index, userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name, final String userId) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(name, userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status, final String userId) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneById(id, userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status, final String userId) {
        if (index == null) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneByIndex(index,userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String name, final Status status,final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneByName(name, userId);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
