package ru.apolyakov.tm.command.project;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectByNameSetStatusCommand extends AbstractProjectCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-set-status-by-name";
    }

    @Override
    public String description() {
        return "Set project status by name";
    }

    @Override
    public void execute() {
        System.out.println("[SETTING STATUS TO PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        System.out.println("to find the project name use the command: project-list");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Project project;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: project = serviceLocator.getProjectService().changeProjectStatusByName(taskName, Status.NOT_STARTED, serviceLocator.getAuthService().getUserId());break;
            case 2: project = serviceLocator.getProjectService().changeProjectStatusByName(taskName, Status.IN_PROGRESS, serviceLocator.getAuthService().getUserId());break;
            case 3: project = serviceLocator.getProjectService().changeProjectStatusByName(taskName, Status.COMPLETE, serviceLocator.getAuthService().getUserId());break;
            default:
                project = null;
        }
        if (project == null) throw new ProjectNotFoundException();
    }

}
