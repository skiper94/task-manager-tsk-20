package ru.apolyakov.tm.command.project;

import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectByNameRemoveCommand extends AbstractProjectCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeOneByName(name, serviceLocator.getAuthService().getUserId());
        if (project == null) throw new ProjectNotFoundException();
    }

}
