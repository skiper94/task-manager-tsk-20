package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProfileUpdateCommand extends AbstractAuthCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "Update user profile";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter first name");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(user.getId(), firstName, lastName, middleName);
    }
}
