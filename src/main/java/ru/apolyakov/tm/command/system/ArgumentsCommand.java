package ru.apolyakov.tm.command.system;

import ru.apolyakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show all arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            final String arg = argument.arg();
            if (arg == null || arg.isEmpty()) continue;
            System.out.println(arg);
        }
    }

}
