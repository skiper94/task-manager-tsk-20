package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByNameSetStatusCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-name";
    }

    @Override
    public String description() {
        return "Set task status by name";
    }

    @Override
    public void execute() {
        System.out.println("[SETTING STATUS TO TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        System.out.println("to find the task name use the command: task-list");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Task task;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: task = serviceLocator.getTaskService().changeTaskStatusByName(taskName, Status.NOT_STARTED, serviceLocator.getAuthService().getUserId());break;
            case 2: task = serviceLocator.getTaskService().changeTaskStatusByName(taskName, Status.IN_PROGRESS, serviceLocator.getAuthService().getUserId());break;
            case 3: task = serviceLocator.getTaskService().changeTaskStatusByName(taskName, Status.COMPLETE, serviceLocator.getAuthService().getUserId());break;
            default:
                task = null;
        }
        if (task == null) throw new TaskNotFoundException();
    }
}
