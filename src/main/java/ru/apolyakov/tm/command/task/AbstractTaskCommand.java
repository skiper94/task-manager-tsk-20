package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(final Task task){
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDispayName());
        System.out.println("PROJECT ID: " + task.getProjectId());
        System.out.println("CREATED: " + task.getCreated());
        System.out.println("STARTED: " + task.getDateStart());
        System.out.println("FINISHED: " + task.getDateFinish());
        System.out.println(System.getProperty("line.separator"));
    }

}
