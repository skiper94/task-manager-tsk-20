package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.exception.entity.EntityNotFoundException;
import ru.apolyakov.tm.model.AbstractEntity;
import ru.apolyakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll(final String userId) {
        List<E> list = new ArrayList<>();
        for (E entity : this.entities) {
            if (userId.equals(entity.getUserId())) list.add(entity);
        }
        return list;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator, final String userId) {
        final List<E> entitiesNew = new ArrayList<>();
        for (E e : entities) if (userId.equals(e.getUserId())) entitiesNew.add(e);
        entitiesNew.sort(comparator);
        return entitiesNew;
    }

    @Override
    public E findOneById(final String id, final String userId) {
        for (final E e : entities) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return e;
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void clear(final String userId) {
        for (final E e: entities) {
            if (userId.equals(e.getUserId())) {
                entities.remove(e);
            }
        }
    }

    @Override
    public void removeOneById(final String id, final String userId){
        remove(findOneById(id, userId));
    }

    @Override
    public void add(final E e) {
        entities.add(e);
    }

    @Override
    public void remove(final E e) {
        entities.remove(e);
    }

}
