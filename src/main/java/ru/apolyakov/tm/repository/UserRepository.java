package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.IUserRepository;
import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean existsByEmail(final String email) {
        for (final User user : entities) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findById(final String id) {
        for (final User user : entities) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : entities) {
            if (email.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public void removeByLogin(final String userId, final String login) {
        remove(findByLogin(login));
    }

    @Override
    public boolean existsByLogin(final String login) {
        for (final User user : entities) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public void setPasswordById(final String userId, final String id, final String password) {
        findOneById(id, userId).setPasswordHash(HashUtil.salt(password));
    }


}
