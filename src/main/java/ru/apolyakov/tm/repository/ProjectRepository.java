package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.model.Project;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findOneByIndex(final Integer index, final String userId){
        List<Project> list = findAll(userId);
        return list.get(index);
    }

    @Override
    public Project findOneByName(final String name, final String userId){
        for (final Project project: entities) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final Integer index, final String userId){
        final Project project = findOneByIndex(index, userId);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String name, final String userId){
        final Project project = findOneByName(name, userId);
        if (project == null) return null;
        remove(project);
        return project;
    }

}
