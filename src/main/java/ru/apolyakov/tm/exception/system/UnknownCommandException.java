package ru.apolyakov.tm.exception.system;

import ru.apolyakov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public  UnknownCommandException(String message) {
        super("Incorrect command \"" + message + "\"");
    }

}
