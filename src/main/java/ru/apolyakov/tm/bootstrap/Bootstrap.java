package ru.apolyakov.tm.bootstrap;

import ru.apolyakov.tm.api.repository.ICommandRepository;
import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.repository.IUserRepository;
import ru.apolyakov.tm.api.service.*;
import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.command.auth.*;
import ru.apolyakov.tm.command.project.*;
import ru.apolyakov.tm.command.system.*;
import ru.apolyakov.tm.command.task.*;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.exception.system.*;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.repository.CommandRepository;
import ru.apolyakov.tm.repository.ProjectRepository;
import ru.apolyakov.tm.repository.TaskRepository;
import ru.apolyakov.tm.repository.UserRepository;
import ru.apolyakov.tm.service.*;
import ru.apolyakov.tm.util.TerminalUtil;

public final class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);

 {
        registry(new ProjectByIdFinishCommand());
        registry(new ProjectByIdRemoveCommand());
        registry(new ProjectByIdSetStatusCommand());
        registry(new ProjectByIdStartCommand());
        registry(new ProjectByIdUpdateCommand());
        registry(new ProjectByIdViewCommand());
        registry(new ProjectByIndexFinishCommand());
        registry(new ProjectByIndexRemoveCommand());
        registry(new ProjectByIndexSetStatusCommand());
        registry(new ProjectByIndexStartCommand());
        registry(new ProjectByIndexUpdateCommand());
        registry(new ProjectByIndexViewCommand());
        registry(new ProjectByNameFinishCommand());
        registry(new ProjectByNameRemoveCommand());
        registry(new ProjectByNameSetStatusCommand());
        registry(new ProjectByNameStartCommand());
        registry(new ProjectByNameUpdateCommand());
        registry(new ProjectByNameViewCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());

        registry(new TaskByIdFinishCommand());
        registry(new TaskByIdRemoveCommand());
        registry(new TaskByIdSetStatusCommand());
        registry(new TaskByIdStartCommand());
        registry(new TaskByIdUnbindCommand());
        registry(new TaskByIdUpdateCommand());
        registry(new TaskByIdViewCommand());
        registry(new TaskByIndexFinishCommand());
        registry(new TaskByIndexRemoveCommand());
        registry(new TaskByIndexSetStatusCommand());
        registry(new TaskByIndexStartCommand());
        registry(new TaskByIndexUpdateCommand());
        registry(new TaskByIndexViewCommand());
        registry(new TaskByNameFinishCommand());
        registry(new TaskByNameRemoveCommand());
        registry(new TaskByNameSetStatusCommand());
        registry(new TaskByNameStartCommand());
        registry(new TaskByNameUpdateCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByProjectIdBindCommand());
        registry(new TaskByProjectIdListCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());

        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new PasswordChangeCommand());
        registry(new ProfileUpdateCommand());
        registry(new ProfileViewCommand());
        registry(new RegistryCommand());

        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());

    }

    {
        String adminId = userService.create("admin", "admin", "apolyakov@tsconsulting.com").setRole(Role.ADMIN).getId();
        String testId = userService.create("user", "user", "adpolyakov@tsconsulting.com").setRole(Role.USER).getId();

        projectService.add("Project 1", "Description of Task 1", adminId).setStatus(Status.NOT_STARTED);
        projectService.add("Project 2", "Description of Task 2", adminId).setStatus(Status.IN_PROGRESS);
        projectService.add("Project 3", "Description of Task 2", adminId).setStatus(Status.COMPLETE);
        taskService.add("Task 1", "Description of Task 1", adminId).setStatus(Status.NOT_STARTED);
        taskService.add("Task 2", "Description of Task 2", adminId).setStatus(Status.IN_PROGRESS);
        taskService.add("Task 2", "Description of Task 2", adminId).setStatus(Status.COMPLETE);
    }

    public void run(final String... args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.err.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) throw new UnknownArgumentException();
        command.execute();
    }

    public void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) throw new UnknownCommandException(cmd);
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}
