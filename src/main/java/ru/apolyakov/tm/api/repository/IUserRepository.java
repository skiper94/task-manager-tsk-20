package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    boolean existsByEmail(String email);

    boolean existsByLogin(String login);

    User findById(String id);

    User findByLogin(String login);

    void removeByLogin(String userId, String login);

    void setPasswordById(String userId, String id, String password);

    User findByEmail(String email);
}
