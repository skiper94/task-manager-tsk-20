package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllTaskByProjectId(String projectId,String userId);

    List<Task> removeAllTaskByProjectId(String projectId,String userId);

    Task bindTaskByProject(String projectId, String taskId,String userId);

    Task unbindTaskByProjectId(String taskId,String userId);

    Task findOneByIndex(Integer index,String userId);

    Task findOneByName(String name,String userId);

    Task removeOneByIndex(Integer index,String userId);

    Task removeOneByName(String name,String userId);


}
