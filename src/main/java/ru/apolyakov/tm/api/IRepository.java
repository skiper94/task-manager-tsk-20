package ru.apolyakov.tm.api;

import ru.apolyakov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository <E extends AbstractEntity> {

    void add(E entity);

    void clear(String userId);

    List<E> findAll(Comparator<E> comparator, String userId);

    List<E> findAll(String userId);

    E findOneById(String id, String userId);

    void removeOneById(String id, String userId);

    void remove(E e);

}
