package ru.apolyakov.tm.api;

import ru.apolyakov.tm.model.AbstractEntity;
import ru.apolyakov.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService <E extends AbstractEntity> {
    List<E> findAll(String userId);
    List<E> findAll(Comparator<E> comparator, String userId);
    E findOneById(String id, String userId);
    void clear(String userId);
    void add(final E entity);
    void removeOneById(String id, String userId);
    void remove(E entity);

}
