package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.api.IService;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.enumerated.Status;
import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    Project add(String name, String description, String userId);

    Project removeOneByName(String name, String userId);

    Project findOneByIndex(Integer index, String userId);

    Project findOneByName(String name, String userId);

    Project updateProjectById(String id, String name, String description, String userId);

    Project removeProjectByIndex(Integer index, String userId);

    Project updateProjectByIndex(Integer index, String name, String description, String userId);

    Project startProjectById(String id, String userId);

    Project startProjectByIndex(Integer index, String userId);

    Project startProjectByName(String name, String userId);

    Project finishProjectById(String id, String userId);

    Project finishProjectByIndex(Integer index, String userId);

    Project finishProjectByName(String name, String userId);

    Project changeProjectStatusById(String id, Status status, String userId);

    Project changeProjectStatusByIndex(Integer index, Status status, String userId);

    Project changeProjectStatusByName(String name, Status status, String userId);

    Project updateProjectByName(String name, String nameNew, String description, String userId);

}
